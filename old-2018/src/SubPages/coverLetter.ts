import { a, create, br, div, span, Innards, RawHTML } from "el-tool";
import SiteColors from "../SiteColors";
import ScalableCurfAug from "../Util/ScalableCurfAug";
import * as MarkdownIt from "markdown-it";
const MD = MarkdownIt({
	html: true,
	xhtmlOut: true,
});

import "./coverLetter.scss";


const myFaceSvg = `
<svg xmlns="http://www.w3.org/2000/svg"
     width="16.2333in" height="20.7667in"
     viewBox="0 0 1461 1869">
    <path id="Path #2"
        vector-effect="non-scaling-stroke"
        d="M 115.00,496.00
           C 132.00,460.00 108.00,349.00 222.00,224.00
             222.00,224.00 217.00,214.00 234.00,191.00
             486.00,-155.00 968.00,74.00 1090.00,322.00
             1174.00,222.00 1226.00,170.00 1238.00,177.00
             1251.00,185.00 1229.00,230.00 1162.00,331.00
             1252.00,286.00 1306.00,315.00 1315.00,335.00
             1316.00,352.00 1217.00,316.00 1162.22,393.56
             1228.00,371.00 1309.00,436.00 1309.00,454.00
             1292.00,464.00 1248.00,416.00 1209.27,429.77
             1297.31,538.73 1315.45,840.47 1372.00,1117.00
             1384.50,1172.50 1350.00,1277.50 1392.00,1352.50
             1263.00,1268.50 1387.50,1121.50 1270.50,1018.00
             1201.50,1367.50 1323.00,1415.50 1444.50,1696.00
             1100.50,1386.50 1201.50,1028.50 1201.50,1028.50
             1147.46,1033.60 1138.00,1241.00 1090.00,1173.00
             960.00,1065.00 1046.50,883.00 967.00,811.00
             986.00,947.00 847.00,1022.00 1005.00,1133.00
             1117.50,1181.50 1271.17,1522.33 1332.67,1816.33
             1335.17,1828.83 1341.00,1849.00 1309.00,1849.00
             1309.00,1850.00 240.00,1850.00 238.00,1849.00
             216.00,1847.00 224.47,1818.83 225.33,1814.33
             245.33,1697.33 325.00,1487.00 442.00,1300.00
             442.00,1300.00 497.00,1145.00 407.00,1120.00
             405.00,1174.00 389.00,1165.00 346.00,1188.00
             236.00,1223.00 78.00,1281.00 143.00,1018.00
             143.00,1018.00 127.00,1027.00 127.00,1027.00
             127.00,1027.00 132.00,977.00 156.00,958.00
             156.00,958.00 141.00,932.00 158.00,902.00
             147.65,895.89 128.00,890.00 126.00,872.00
             107.00,873.00 109.24,874.06 105.00,864.00
             100.00,845.00 103.00,847.00 110.00,813.00
             97.00,795.00 -11.00,820.00 31.00,721.00
             67.00,670.00 56.00,658.00 71.00,628.00
             90.00,593.00 145.00,556.00 108.00,527.00
             93.00,513.00 107.00,509.00 115.00,496.00 Z" />
</svg>
`


const colorsStyle = document.createElement("style");
colorsStyle.innerHTML = [
	":root {",
	...Array.from(Object.keys(SiteColors)).map((key) => `--${key}: ${SiteColors[key]};`),
	"}",
].join("\n");
document.head.appendChild(colorsStyle);

export function fromMD(str: string): RawHTML {
	return {hackableHTML: MD.render(str)};
}


const versions: {[key: string]: {name: string; blurb: Innards}} = {
	flixBus: {
		name: "FLIXBUS",
		blurb: fromMD(`
One of favorite parts of going to college in Portland Oregon was the metro system there, TriMet.  It was the first city I'd ever lived in where you didn't need a car to survive.  Everything always felt so close, just a few chapters in a book away, and planning with friends could always be in the moment.  Nowadays I live in Austin TX, and the only way to survive is to drive *a lot*.  For the most part, I don't think of it, there are some very unique benefits that come from everyone having so much land (space for large scale wood and metal art is more available), but when I do remember riding a proper metro, it is always longingly.  So working with busses, especially green busses, sounds amazing!

Further, I've always wanted to live in Europe, Germany especially.  My childhood friends (twins) took a trip around Europe and returned to tell me that Germany was the cleanest, most orderly, effecient, well engineered place they'd ever seen.  I've been dreaming of seeing it for myself since.  Enough so that instead of taking one of the languages my High School offered, I commuted an hour twice a week to learn German at the local community college.

I have also done a lot of volunteer and community building work which you can see in my portfolio.  Ethics, learning, and adventure have always been some of my greatest motivators, and this job appears to have all of them, so I really hope to hear from you!
		`),
	},
	owlchemy: {
		name: "Owlchemy",
		blurb: fromMD(`
First off, I loved job simulator, and also pretty much everything made by Dan Harmon or in reference to things made by him.  So you guys have been on my radar for a while, and I'm very excited to find that you're based in Austin.  Anyways, a job there, I want it, and I think I'm a great fit for some reasons which I will list:

1. I'm really talented at pretty much everything I do.  I have no idea why, I don't think I'm that smart, but for whatever reason things that seem really obvious to me strike others as brilliant.  I'm usually just trying to be lazy, or make sure the right message is coming across.  I know this is kind of a "humble brag," but it's true and I'm kind of proud of it.
2. I've been making games since I was 10, programming for over a decade, doing 3d modeling for games since source engine came out, and have even taken these skills and applied them to multiple real world builds (http://sephreed.me/#Cononagon).  It may not be for your company, but (barring untimely death) I will some day be a great game developer.  In fact, I'm developing a board game right now called Synergy, and it's pretty much a tongue in cheek office simulator with some backstabbing mechanics thrown in for realism.
3. I think very, very different (eg. I don't believe in opposites, and utilize the concept of paradox instead).  Unfortunately, this generally means I look nothing like the thing anyone is ever looking for, but I can assure you that's a benefit.  There are innumerable ways in which getting a unique perspective is helpful, most notably depth perception.  I also draw insparation from parts of the world and life that other people would never have had access to, and having been a van dwelling adventurer/traveler really filled my culture and stories bank, specifically as relates to strange people and places.  Nobody ever seems to think about the stories and cultures bank because you can't get a degree for it, but imo that's where inspiration comes.
4. I've never not performed exceptionally in any position I'm given.  Due to the fact that I look weird, and think weird, and ultimately live weird... due to being weird, I really have to out perform the position every chance I get to get ahead.  Which is simultaneously a blessing and a curse.  Anyways, you can message any of my old jobs and ask them how I performed, the answer for all will be somewhere in the neighborhood of "exceptionally."  My last boss even called me "more than employee material, but co-founder material."

So, there's some bragging for yeh.  Hope it piques your interest - Seph
		`),
	}
}

const output = versions.owlchemy;

let interest: HTMLElement;
let portfolio: HTMLElement;
const pageEl = div("CoverLetter", [
	div("AboutMe", [
		div("MyFace", [{ hackableHTML: myFaceSvg }]),
		div("Bio", [
			div("Tail"),
			div("Content", [
				"Hello.  My name is ",
				span("MyName", "Seph Reed"),
				" and I'm trying to understand the system as a whole. My interests and skills are all over the place, I rarely consider odd jobs a waste of time, and I have a habit of being able to fill suprising roles.  In all fields, I'm a relentless learner."
			]),
		]),
	]),
	interest = div("Interest", [
		div("Header", `Why I'm intersted in ${output.name}`),
		div("Spacer"),
		div("Blurb", output.blurb),
	]),
	portfolio = div("Portfolio", [
		"If you want to dig in deeper on who I am and what I'm capable of, check out my portfolio: ",
		a({href: "http://SephReed.me"}, "http://SephReed.me"),
	]),
]);

new ScalableCurfAug(interest, {
	curfNw: {width: 25, height: 25},
	curfSe: {width: 12.5, height: 12.5},
	strokeStyle:  "#5e6e6c",
	strokeWidth: 2,
	fillStyle: "black",
});

new ScalableCurfAug(portfolio, {
	curfNw: {width: 12.5, height: 12.5},
	curfNe: {width: 12.5, height: 12.5},
	curfSw: {width: 12.5, height: 12.5},
	curfSe: {width: 12.5, height: 12.5},
	// fillStyle: SiteColors.timelineTitleBg,
	strokeStyle: "black",
	strokeWidth: 2,
});

export default function () {
	document.addEventListener("DOMContentLoaded", () => {
		document.getElementById("AppWrapper").appendChild(pageEl);
	})
}