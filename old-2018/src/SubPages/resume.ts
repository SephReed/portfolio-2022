import { a, create, br, div, span, Innards } from "el-tool";
import SiteColors from "../SiteColors";
import ScalableCurfAug from "../Util/ScalableCurfAug";

import "./resume.scss";

const colorsStyle = document.createElement("style");
colorsStyle.innerHTML = [
	":root {",
	...Array.from(Object.keys(SiteColors)).map((key) => `--${key}: ${SiteColors[key]};`),
	"}",
].join("\n");
document.head.appendChild(colorsStyle);

const createLink = (href) => a({href: `http://${href}`}, href);

const employmentHistory: Array<{positions: Innards, company: Innards, time: Innards, info: Innards}> = [{
	positions: ["Frontend Dev,", br(), "UX Team"],
	company: ["Coder Technologies", br(), createLink("sephreed.me#CoderComDev")],
	time: "Jan-Oct 2018",
	info: [{hackableHTML: `John A. Entwistle - ceo<small>jae@coder.com</small><br>
		Joanne Kimmilnes - designer<small>info@joannekimmilnes.com</small>`}],
},{
	positions: ["Build Lead,", br(), "Codesigner"],
	company: ["Austin Artistic Reconstruction LLC", br(), createLink("sephreed.me#Cononagon")],
	time: "Oct-May 2018",
	info: [{hackableHTML: `Adam Rice - board member<small>adamrice@8stars.org</small>`}],
},{
	positions: ["Full Stack Dev,", br(), "Fabricator"],
	company: ["Contract Work", br(), "(Custom interactive light)", br(), createLink("sephreed.me#DadLight")],
	time: "Jan-Aug 2017",
	info: [{hackableHTML: `Sally Hall - client<small>sally.hall@austincenterfordesign.com</small>`}],
},{
	positions: ["Build Lead,", br(), "Codesigner"],
	company: ["Austin Artistic Reconstruction LLC", br(), createLink("sephreed.me#PlaywoodPalace")],
	time: "Oct-May 2017",
	info: [{hackableHTML: `Stephanie Vyborny - artist facilitator<small>the.lost.muse@gmail.com</small>`}],
},{
	positions: ["Web Dev,", br(), "3D Modeling"],
	company: ["Stash Crypto", br(), createLink("sephreed.me#Stash")],
	time: "2016",
	info: [{hackableHTML: `Anonymity desired`}],
},{
	positions: ["Full Stack Dev"],
	company: ["Realme App", br(), "(Design School Final Project)"],
	time: "Spring 2016",
	info: [{hackableHTML: `Misty Nickle - client<small>mistynickle@gmail.com</small>`}],
},{
	positions: ["Instructor,", br(), "Fabricator"],
	company: ["Austin Tinkering School"],
	time: "Summer 2016",
	info: [{hackableHTML: `Kami Wilt - owner<small>kami@austin.tinkeringschool.com</small>`}],
},{
	positions: ["Forest Service,", br(), "Team Leading"],
	company: ["	Northwest Youth Corps.", br(), "Leadership Development"],
	time: "Fall 2014",
	info: [{hackableHTML: `Jon Zintell - leader<small>jon@nwyouthcorps.org<small>`}],
},
]


const skills: Array<[number, string]> = [
	[3, "Typescript (2018)"],
	[2, "Sass (2018)"],
	[3, "React (2018)"],
	[1, "Webpack (2018)"],
	[2, "Arduino / FastLED (2018)"],
	[1, "C (2018)"],
	[2, "Circuitry (2017)"],
	[2, "WebWorkers (2017)"],
	[2, "Signal Processing (FFT)(2017)"],
	[3, "Volunteer Leadership (2017)"],
	[3, "Project Management (Foreman)(2017)"],
	[3, "Web Audio API (2017)"],
	[2, "Basic Cryptology (AES-EBC)(2016)"],
	[2, "Blender 3d (2016)"],
	[3, "Teaching (2016)"],
	[2, "Three.js / WebGl (2016)"],
	[2, "Mapping / Geolocation API (2016)"],
	[2, "Home Renovation (2016)"],
	[2, "Carpentry (2014)"],
	[3, "Leadership (2014)"],
	[3, "Bitwig (DAW)(2014)"],
	[3, "BS in Computer Science from Portland State University (2013)"],
	[2, "Cooking (2012)"],
	[1, "Python (2012)"],
	[3, "Svg / Vector Art (2012)"],
	[2, "SQL (2012)"],
	[3, "CSS (2011)"],
	[2, "Jazz Theory (2010)"],
	[2, "PHP (2010)"],
	[3, "Javascript (2010)"],
	[2, "Fire Performance (2009)"],
	[2, "C++ (2009)"],
	[2, "Bash (Unix/Linux)(2009)"],
	[2, "Java (2008)"],
	[3, "Photoshop / Gimp (2007)"],
	[3, "SketchUp (CAD)(2007)"],
	// [1, "Auto-Cad (2007)"],
	[3, "Digital Audio Workspaces (2007)"],
	[2, "3d Modeling (2004)"],
	[2, "Guitar (2004)"],
	[3, "HTML (2002)"],
	[3, "Piano (2000)"],
	[1, "Sandbox Java (2000)"],
]


const forPrint = window.location.search.indexOf("PrintVersion") !== -1;

let table: HTMLElement;
let portfolio: HTMLElement;
const app = div(`Resume ${forPrint ? "--variant_print_version" : ""}`, [
	div("LeftCol", [
		div("Name", [
			div("MyName", "Seph Reed"),
			"legal name: Scott Jaromin",
		]),
		div("Contact", [
		`ShadyWillowCreek@gmail`,br(),
		`(737)-529-5031`,
		]),
		div("Positions", [
			{hackableHTML:
				`Full Stack Developer
				Project Lead
				UI/UX Designer
				2D/3D Digital Artist
				Fabricator
				Producer/Jazz Pianist`.split("\n").join("<br>")
			}
		]),
		div("SelfIntro", 
			`Hello.  My name is Seph and I'm trying to understand the system as a whole. My interests and skills are all over the place, I rarely consider odd jobs a waste of time, and I have a habit of being able to fill suprising roles.  In all fields, I'm a relentless learner.

			My core values are self-respect, humor, integrity, self-awareness, and competency. My personality type is somewhere between INTJ and ENTJ.

			Ideally, I would like an employer that is willing to push boundaries and strives to create a symbiotic relationship between itself and its customers.

			Idealistically, I would like to work with art, music, and those kinds of things which impact people in deep, long lasting ways.

			Overall, I'd just like to solve some problems and have a good time while I'm at it.`.split("\n\n")
			.map((blurb) => div("Blurb", blurb)),
		)
	]),
	div("RightCol", [
		div("PrintLink", [
			"Printable version: ",
			a({href: "http://sephreed.me/SephReed-Resume(printer).pdf"}, "sephreed.me/SephReed-Resume(printer).pdf"),
		]),
		div("Title", "Employment History".toUpperCase()),
		table = div("EmploymentTable", [
			// ...["Positions", "Company / Info Link", "Time", "Contact / References"].map((title) => div("TableHeader", title)),
			...["Positions", "Company / Info Link", "Time"].map((title) => div("TableHeader", title)),
			div("Spacer header"),
			...(() => {
				const rows = [];
				employmentHistory.forEach((employment, index) => {
					if (index > 0) { rows.push(div("Spacer")); }
					const datumList = Object.keys(employment).map((prop) => div("Datum", employment[prop]));
					datumList.pop();
					rows.push(div("Employment", datumList));
				});
				return rows;
			})(),
		]),
		portfolio = div("Portfolio", [
			"My portfolio showcases everything above and below",
			a({href: "http://SephReed.me"}, "http://SephReed.me"),
		]),
		div("Skills", [
			div("Title", "Skills Timeline".toUpperCase()),
			div("Desc", "The following is a timeline of various skills I've acquired. The date denotes the time of my first project involving the skill. The stars represent beginner, intermediate, and expert levels."),
			div("SkillList", skills.map((skill) => {
				let stars: string;
				switch(skill[0]) {
					case 1: stars = "<span>&#9733;</span>&#9734;&#9734"; break;
					case 2: stars = "<span>&#9733;&#9733;</span>&#9734"; break;
					case 3: stars = "<span>&#9733;&#9733;&#9733</span>"; break;
				}
				return div("Skill", [
					span("Stars", [{hackableHTML: stars}]),
					skill[1],
				]);
			}))
		])
	]),
]);

new ScalableCurfAug(table, {
	curfNe: {width: 25, height: 25},
	curfSw: {width: 12.5, height: 12.5},
	strokeStyle:  forPrint ? "black" :SiteColors.timelineStroke,
	strokeWidth: 2,
	fillStyle: forPrint ? null : SiteColors.timelineFill,
});

new ScalableCurfAug(portfolio, {
	curfNw: {width: 12.5, height: 12.5},
	curfNe: {width: 12.5, height: 12.5},
	curfSw: {width: 12.5, height: 12.5},
	curfSe: {width: 12.5, height: 12.5},
	fillStyle: forPrint ? null : SiteColors.timelineStroke,
	strokeStyle: forPrint ? SiteColors.timelineStroke : null,
	strokeWidth: forPrint ? 3 : null,
});


export default function () {
	document.addEventListener("DOMContentLoaded", () => {
		document.getElementById("AppWrapper").appendChild(app);
	})
}
