import resizeHandler from "./resizeHandler";

export interface IArgs {
	pxWidth: number;
	pxHeight: number;
	limitWidth: "parent";
};

export default class GridAug {
	private domNode: HTMLElement;
	private args: IArgs;
	// private requestedFrame: number;

	constructor(domNode: HTMLElement, args: IArgs) {
		this.domNode = domNode;
		this.args = args;

		resizeHandler(this.updateGrid.bind(this));
	}

	private updateGrid() {
		const { pxWidth, pxHeight } = this.args;

		const widthLimit = this.domNode.parentElement.offsetWidth;
		const griddedWidth = widthLimit - (widthLimit % pxWidth);
		this.domNode.style.width = griddedWidth + "px";

		return new Promise((resolve) => {
			requestAnimationFrame(() => requestAnimationFrame(() => {
				const heightLimit = this.domNode.offsetHeight;
				if (heightLimit % pxHeight !== 0) {
					const griddedHeight = (heightLimit - (heightLimit % pxHeight) + pxHeight);
					this.domNode.style.height = griddedHeight + "px";
				}
				resolve();
			}));
		})
	}
}