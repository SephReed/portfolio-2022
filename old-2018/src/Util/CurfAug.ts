// import resizeHandler from "./resizeHandler";
import observeSize from "./observeSize";

import "./CurfAug.scss";

export type Curf = {height: number; width: number};

export interface IArgs {
	curfNw?: Curf;
	curfNe?: Curf;
	curfSw?: Curf;
	curfSe?: Curf;
	contentElement?: HTMLElement;
	height?: number;
	width?: number;
}

const SVG_NS = "http://www.w3.org/2000/svg";

export default class CurfAug {
	private readonly path: SVGPathElement;
	public readonly svg: SVGElement;
	public readonly domNode: HTMLElement;
	private args: IArgs;
	

	public constructor(domNode: HTMLElement, args: IArgs) {
		this.domNode = domNode;
		this.args = args;
		this.svg = document.createElementNS(SVG_NS, "svg");
		this.svg.classList.add("CurfAugBorder");
		this.path = document.createElementNS(SVG_NS, "path");
		this.svg.appendChild(this.path);
		domNode.appendChild(this.svg);

		observeSize(this.domNode, () => this.redraw())
	}

	public patchArgs(args: IArgs) {
		for (let key in args) {
			this.args[key] = args[key];
		}
		this.redraw();
	}

	private redraw() {
		const {curfNw, curfNe, curfSw, curfSe } = this.args;
		const height = this.args.height || this.domNode.offsetHeight;
		const width = this.args.width || this.domNode.offsetWidth;

		this.svg.setAttribute("width", width + "px");
		this.svg.setAttribute("height", height + "px");
		this.svg.setAttribute("viewport", `0 0 ${width} ${height}`);

		const d = [
			...(curfNw ? [
				`M 0 ${curfNw.height}`,
				`L ${curfNw.width} 0`,
			] : [
				"M 0 0"
			]),
			
			...(curfNe ? [
				`L ${width - curfNe.width} 0`,
				`L ${width} ${curfNe.height}`,
			] : [
				`L ${width} 0`
			]),

			...(curfSe ? [
				`L ${width} ${height - curfSe.height}`,
				`L ${width - curfSe.width} ${height}`,
			] : [
				`L ${width} ${height}`
			]),			

			...(curfSw ? [
				`L ${curfSw.width} ${height}`,
				`L 0 ${height - curfSw.height}`,
			] : [
				`L 0 ${width}`
			]),

			"z",				
		].join(" ");

		this.path.setAttribute("d", d);
	}
}