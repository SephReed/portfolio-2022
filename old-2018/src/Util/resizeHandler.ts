export default function resizeHandler(cb: () => void | Promise<void>) {
	let requestedFrame;
	const doUpdate = () => {
		if (requestedFrame) { return; }
		requestedFrame = window.requestAnimationFrame(() => {
			const promise = cb() || Promise.resolve();
			promise.then(() => requestedFrame = undefined);	
		});
	};

	window.addEventListener("resize", doUpdate);

	if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", doUpdate);
	} else {  
	  doUpdate();
	}
}