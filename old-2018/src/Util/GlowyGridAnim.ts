// import resizeHandler from "./resizeHandler";
// import observeSize from "./observeSize";

export interface IArgs {
	auraColor: string;
	auraRadius: number;
	mouseMoveListenerTarget?: HTMLElement;
}

export default class GridAuraAnim {
	private args: IArgs;
	public readonly domNode: HTMLElement;

	private gridCanvas: HTMLCanvasElement;
	private gridDiv: HTMLElement;
	private gridImg: HTMLImageElement;

	private auraCanvas: HTMLCanvasElement;
	private auraPainter: CanvasRenderingContext2D;
	private auraImg: HTMLImageElement;
	private canvasSize: number;
	private auraPos: {x: number; y: number};
	private frameRequest: number;
	private mouseMoveListener: (event: MouseEvent) => void;

	constructor(args: IArgs) {
		this.args = args;

		this.domNode = document.createElement("div");
		this.domNode.className = "GridAuraAnim";
		this.canvasSize = (this.args.auraRadius * 2) + 25;

		this.initAuraImg();
		this.initGridImg();

		this.domNode.appendChild(this.getnitGridDiv());
		this.domNode.appendChild(this.getnitAuraCanvas());

		// observeSize(this.domNode, () => {
		// 	// this.auraCanvas.width = this.domNode.offsetWidth;
		// 	// this.auraCanvas.height = this.domNode.offsetHeight;

		// 	// this.redrawGrid();
		// 	this.redrawAura();
		// });

		
		// mouseMoveTarget.addEventListener("mousemove", (event) => {
		// 	const clientRect = (event.currentTarget as HTMLElement).getBoundingClientRect();
		// 	const x = event.x - clientRect.left;
		// 	const y = event.y - clientRect.top;
		// 	this.auraPos = {x, y};
		// 	this.redrawAura();
		// });

		const onMovement = (event: MouseEvent | TouchEvent) => {
			let clientX, clientY;
			if (event instanceof MouseEvent) {
				clientX = event.clientX;
				clientY = event.clientY;
			} else {
				clientX = event.touches[0].clientX;
				clientY = event.touches[0].clientY;
			}
			const bounds = (event.currentTarget as HTMLElement).getBoundingClientRect();

			this.auraPos = {
				x: clientX - bounds.left,
				y: clientY - bounds.top,
			};
			this.redrawAura();
		};

		const mouseMoveTarget = args.mouseMoveListenerTarget || this.auraCanvas;
		mouseMoveTarget.addEventListener("mousemove", onMovement);
		mouseMoveTarget.addEventListener("touchmove", onMovement);

		// mouseMoveTarget.addEventListener("mouseleave", (event) => {
		// 	this.auraPos = undefined;
		// 	this.redrawAura();
		// });
	}

	private getnitGridDiv() {
		if (this.gridDiv === undefined) { 
			this.gridDiv = document.createElement("div");
			this.gridDiv.className = "Grid";
			this.gridDiv.style.backgroundImage = `url(${this.gridImg.src})`
		}
		return this.gridDiv;
	}

	private getnitAuraCanvas() {
		if (this.auraCanvas === undefined) { 
			this.auraCanvas = document.createElement("canvas");
			this.auraCanvas.className = "Aura";
			this.auraCanvas.width = this.auraCanvas.height = this.canvasSize;
			this.auraPainter = this.auraCanvas.getContext("2d");
		}
		return this.auraCanvas;
	}

	private initAuraImg() {
		this.auraImg = document.createElement("img");
		const radius = this.args.auraRadius;
		const diameter = 2 * radius;

		const mouseAuraCanvas = document.createElement("canvas");
		mouseAuraCanvas.width = mouseAuraCanvas.height = diameter;
		const painter = mouseAuraCanvas.getContext("2d");

		const grad = painter.createRadialGradient(radius,radius,0,radius,radius,radius);
		grad.addColorStop(0, this.args.auraColor);
		grad.addColorStop(1, "transparent");

		painter.fillStyle = grad;
		painter.fillRect(0, 0, mouseAuraCanvas.width, mouseAuraCanvas.height);
		this.auraImg.src = mouseAuraCanvas.toDataURL();
	}

	private initGridImg() {
		const gridCanvas = document.createElement("canvas");
		const size = this.canvasSize;
		gridCanvas.width = gridCanvas.height = size;
		const painter = gridCanvas.getContext("2d");
		painter.fillStyle = "white";
		for (let row = 0; row < size; row += 25) {
			for (let col = 0; col < size; col += 25) {
				painter.globalAlpha = 0.4;
				if (row === 0) {
					painter.fillRect(col, 0, 1, size);
				}
				if (col === 0) {
					painter.fillRect(0, row, size, 1);
				}

				painter.globalAlpha = 1;
				painter.fillRect(col-3, row, 7, 1);
				painter.fillRect(col, row-3, 1, 7);
			}	
		}

		this.gridImg = document.createElement("img");
		this.gridImg.src = gridCanvas.toDataURL();
	}

	// private redrawGrid() {
	// 	const canvas = this.gridCanvas;
	// 	const painter = canvas.getContext("2d");
	// 	painter.fillStyle = "white";
	// 	for (let row = 0; row < canvas.offsetHeight; row += 25) {
	// 		for (let col = 0; col < canvas.offsetWidth; col += 25) {
	// 			painter.globalAlpha = 0.4;
	// 			if (row === 0) {
	// 				painter.fillRect(col, 0, 1, canvas.offsetHeight);
	// 			}
	// 			if (col === 0) {
	// 				painter.fillRect(0, row, canvas.offsetWidth, 1);
	// 			}

	// 			painter.globalAlpha = 1;
	// 			painter.fillRect(col-3, row, 7, 1);
	// 			painter.fillRect(col, row-3, 1, 7);
	// 		}	
	// 	}

	// 	this.gridImg.src = canvas.toDataURL();
	// }

	private redrawAura() {
		if (this.frameRequest) { return; }
		this.frameRequest = requestAnimationFrame(() => {
			this.frameRequest = undefined;
			const {auraImg, auraPos, args, auraCanvas, gridImg, auraPainter} = this;
			const radius = args.auraRadius;
			const diameter = 2 * radius;
			
			if (this.auraPos === undefined) { return; }

			const x = auraPos.x - radius;
			const y = auraPos.y - radius;

			auraCanvas.style.top = (y - (y%25)) + "px";
			auraCanvas.style.left = (x - (x%25)) + "px";

			auraPainter.fillStyle = "red";
			auraPainter.drawImage(gridImg, 0, 0);
			auraPainter.save();
			auraPainter.globalCompositeOperation = "source-in";
			auraPainter.drawImage(auraImg, x%25, y%25);
			auraPainter.restore();
		})
	}
}