import resizeHandler from "./resizeHandler";

import "./CurfAug.scss";

export type Curf = {height: number; width: number};

export interface IArgs {
	curfNw?: Curf;
	curfNe?: Curf;
	curfSw?: Curf;
	curfSe?: Curf;
	contentElement?: HTMLElement;
	strokeWidth?: number;
	strokeStyle?: string;
	fillStyle?: string;
	// height?: number;
	// width?: number;
}

const SVG_NS = "http://www.w3.org/2000/svg";

export default class CurfAug {
	public readonly bgLayer: HTMLDivElement;
	public readonly domNode: HTMLElement;
	private canvas: HTMLCanvasElement;
	private args: IArgs;
	

	public constructor(domNode: HTMLElement, args: IArgs) {
		this.domNode = domNode;
		this.args = args;
		this.bgLayer = document.createElement("div");
		this.bgLayer.className = "CurfAugBorder";
		this.domNode.appendChild(this.bgLayer);
		this.canvas = document.createElement("canvas");

		this.redraw();
	}

	public patchArgs(args: IArgs) {
		for (let key in args) {
			this.args[key] = args[key];
		}
		this.redraw();
	}

	private redraw() {
		const {curfNw, curfNe, curfSw, curfSe, strokeStyle, fillStyle } = this.args;
		const strokeWidth = this.args.strokeWidth || 0;

		const getMax = (curfA: Curf, curfB: Curf, prop: "height" | "width") => {
			return Math.max(
				(curfA || {})[prop] || 0, 
				(curfB || {})[prop] || 0,
				strokeWidth || 0,
			);
		}
		const maxTop = Math.ceil(getMax(curfNw, curfNe, "height") + strokeWidth);
		const maxBottom = Math.ceil(getMax(curfSw, curfSe, "height") + strokeWidth);
		const maxLeft = Math.ceil(getMax(curfNw, curfSw, "width") + strokeWidth);
		const maxRight = Math.ceil(getMax(curfNe, curfSe, "width") + strokeWidth);

		const height = this.canvas.height = maxTop + 20 + maxBottom;
		const width = this.canvas.width = maxLeft + 20 + maxRight;

		const painter = this.canvas.getContext("2d");
		painter.beginPath();
		let start: [number, number];
		if (curfNw) {
			start = [0, curfNw.height];
			painter.moveTo(start[0], start[1]);
			painter.lineTo(curfNw.width, 0);
		} else {
			start = [0, 0];
			painter.moveTo(start[0], start[1]);
		}

		if (curfNe) {
			painter.lineTo(width - curfNe.width, 0);
			painter.lineTo(width, curfNe.height);
		} else {
			painter.lineTo(width, 0);
		}

		if (curfSe) {
			painter.lineTo(width, height - curfSe.height);
			painter.lineTo(width - curfSe.width, height);
		} else {
			painter.lineTo(width, height);
		}

		if (curfSw) {
			painter.lineTo(curfSw.width, height);
			painter.lineTo(0, height - curfSw.height);
		} else {
			painter.lineTo(0, width);
		}
	
		painter.lineTo(start[0], start[1]);
		
		if (fillStyle) {
			painter.fillStyle = fillStyle;
			painter.fill();

			const fillDiv = document.createElement("div");
			fillDiv.className = "FillDiv";
			fillDiv.style.background = fillStyle;
			this.bgLayer.appendChild(fillDiv);
		}

		if (strokeWidth) {
			painter.lineWidth = strokeWidth;
			painter.strokeStyle = strokeStyle || "";
			painter.stroke();
		}

		const sizes = [maxTop, maxRight, maxBottom, maxLeft];
		this.bgLayer.style.borderStyle = "solid";
		this.bgLayer.style.borderWidth = sizes.map((size) => size+"px").join(" ");
		this.bgLayer.style.borderImage = `url(${this.canvas.toDataURL()}) ${sizes.join(" ")} repeat`;
	}
}