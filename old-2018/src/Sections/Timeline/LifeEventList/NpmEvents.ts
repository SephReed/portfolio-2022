import {div} from "el-tool";
import {ILifeEvent, twoColLayout, extLink, fromMD} from "./LifeEvent"


const NpmEvents: ILifeEvent[] = [];

/*********************
*   El-tool
**********************/
NpmEvents.push({
	id: "el-tool",
	startDate: new Date("Jun 1 2018"),
	tags: ["featured", "work", "development"],
	title: "El-tool",
	desc: () => fromMD(`
**Skills:** Browser Backend/Native Code Optimization, Framework Design, KISS Principles

----

**Info:** El-tool is a DOM renderer, similar to React or Vue, but less limited, lighter, and faster.  In fact, El-tool is the fastest interactive dom renderer ("framework") out there.  I didn't expect it would be when I wrote it, I really just wanted something simple and syntactically pretty to tersify native code.  But (on my birthday) I decided to do some benchmarks and found that the only way to render html faster is with raw html, and ***there is currently no faster way to render an interactive dom*** out there.  This is what makes it so great:

<table class="table_variant_horizontal_lines" style="margin: 25px 75px;">
	<tr><th>Design Choice</th><th>Benefits</th></tr>
	<tr><td>Generates complete sub trees before appending</td><td><ul>
		<li>Eliminates thrashing (reflows)</li>
		<li>Holds off premature bubbling</li>
		<li>Is inherently faster due to native code nuances *</li>
	</ul></td></tr>
	<tr><td>Relies primarily on a single recursive function</td><td><ul>
		<li>Minimizes js parsing</li>
		<li>Maximizes temporal locality reducing fetch/execute times **</li>
	</ul></td></tr>
	<tr><td>Nested/layered syntax</td><td><ul>
		<li>Syntactically analagous to html</li>
		<li>Quicker code comprehension/navigation</li>
	</ul></td></tr>
	<tr><td>Has render-time HTMLElement reference catching</td><td><ul>
		<li>Eliminates need for slow, JQuery like reference finding</li>
		<li>Brings ui structure and function together, stopping spaghetti code</li>
		<li>Allows for event listeners to be ready by first paint</li>
	</ul></td></tr>
	<tr><td>Uses only vanilla code</td><td><ul>
		<li>Super backwards compatible</li>
		<li>Easily understandable for beginners</li>
	</ul></td></tr>
	<tr><td>Uses as little code as possible</td><td><ul>  
		<li>Super lightweight (<250 lines unminified with comments)</li>
		<li>Can be rewritten and modified by anyone, painlessly, in less than an hour</li>
	</ul></td></tr>
<table>

<small>I developed this project while working at Coder, and was given ownership of it when massive refactors led to it being no longer applicable.  Because El-tool is a one man project with no advertising budget, there's very little chance this tech will take root or spread any time soon, regardless of its promising benchmarks.</small>

---

<div class="Links">
	${extLink(
		"Npm package page", 
		"https://www.npmjs.com/package/el-tool",
	)}
	<br>
	${extLink(
		"* The fastest/slowest ways to append elements to DOM", 
		"https://howchoo.com/g/mmu0nguznjg/learn-the-slow-and-fast-way-to-append-elements-to-the-dom",
	)}
	<br>
	${extLink(
		"** Description of temporal/spacial locality", 
		"https://stackoverflow.com/questions/7638932/what-is-locality-of-reference",
	)}
</div>
	`),
});



/*********************
*   LUMAS
**********************/
NpmEvents.push({
	id: "lumas",
	startDate: new Date("July 1 2018"),
	tags: ["featured", "work", "design"],
	title: "Lumas",
	desc: () => twoColLayout(
		[
			{ hackableHTML: extLink(
				`<img class="--bordered" src="${require('../../../../assets/Lumas-demo.gif')}">`,
				"https://www.youtube.com/watch?v=47XC0WGpAQU", 
			)},
		],
		// 
		fromMD(`
**Skills:** Scientific Color Theory, UI Abstraction, Css Variables, JavaScript

----

**Info:** 

Lumas is a custom theming tool designed to be able to take arbitrary color palettes and turn them into readable, consistent UIs.  It does this by mixing and matching color sets and luminosity mappings.  The video below does a great job of showing these steps it takes when creating a theme:

1. First, organize the random colors of a color set by brightness.
2. Then, make a gradient from black (0% luminosity) through the colors (in order of luminosity) to white (100% luminosity).
3. Finally, use luminosity mapped css variables such as foreground, background, faded text, and border color to specific luminosity positions within the gradient.  

By keeping luminosity constant, contrast remains stable (regardless of changes to overall hue or saturation), and contrast is the most important part of any UI.  Calculating and setting luminosity is much more difficult than Value in HSV or Lightness in HSL color spaces.*
<br>
<br>
<small>I developed this project while working at Coder, and was given ownership of it when massive refactors led to it being no longer applicable.</small>

---

<div class="Links">
	${extLink(
		"Screencap of Lumas demo", 
		"https://www.youtube.com/watch?v=47XC0WGpAQU",
	)}
	<br>
	${extLink(
		"* CIELAB color space", 
		"https://en.wikipedia.org/wiki/CIELAB_color_space#CIELAB%E2%80%93CIEXYZ_conversions",
	)}
</div>
		`),
	),
});



// /*********************
// *   TEMPLATE
// **********************/
// NpmEvents.push({
// 	id: "Stash",
// 	startDate: new Date("Oct 8 2016"),
// 	tags: ["featured", "work", "development", "design"],
// 	title: "",
// 	desc: twoColLayout(
// 		[
// 			{ hackableHTML: extLink(
// 				`<img class="--clip_curf_all_sm --add_bg_light" src="${require('../../../../assets/StashLogo.png')}">`,
// 				"https://stashcrypto.com", 
// 			)},
// 		],
// 		// 
// 		fromMD(`
// **Skills:** 

// ----

// **Info:** 

// ---

// <div class="Links">
// 	${extLink(
// 		"StashCrypto.com", 
// 		"https://stashcrypto.com",
// 	)}
// </div>
// 		`),
// 	),
// });

export default NpmEvents;