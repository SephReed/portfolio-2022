import { create, br, div, span, Innards, Child, RawHTML } from "el-tool";
// import { markdown } from "markdown";
import * as MarkdownIt from "markdown-it";
const MD = MarkdownIt({
	html: true,
	xhtmlOut: true,
});

export type Tags = "featured" | "design" | "fabrication" | "development" | "audio" 
| "imagery" | "social" | "leadership" | "writing" | "3d" | "interactive" | "work";

const tagsObject: {[key in Tags]: null} = {
	featured: null,
	design: null,
	fabrication: null,
	development: null,
	audio: null,
	imagery: null,
	social: null,
	leadership: null,
	writing: null,
	"3d": null,
	interactive: null,
	work: null,
}
const tagList: Tags[] = Array.from(Object.keys(tagsObject)) as any;
export { tagList };

export interface ILifeEvent {
	id: string;
	startDate: Date,
	endDate?: Date,
	tags: Tags[];
	title: HTMLElement | string;
	desc: () => (Innards | Child);
}

export function fromMD(str: string): RawHTML {
	return {hackableHTML: MD.render(str)};
}

export function extLink(innerText: string, href: string) {
	return `<a target="_blank" href=${href}>${innerText}</a>`;
}

export function twoColLayout(left: Innards | Child, right: Innards | Child) {
	return div("TwoColLayout", [
		div("LeftCol", Array.isArray(left) ? left : [left]), 
		div("RightCol", Array.isArray(right) ? right : [right])
	]);
}