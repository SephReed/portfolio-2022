import { addClass, create, br, div, span } from "el-tool";
import LifeEventList, {tagList, Tags, ILifeEvent} from "./LifeEventList";
// import GridAug from "../Util/GridAug";
import GridSizingWrapper from "../../Util/GridSizingWrapper";
import CurfAug, {Curf} from "../../Util/CurfAug";
import ScalableCurfAug from "../../Util/ScalableCurfAug";
import GlowyGridAnim from "../../Util/GlowyGridAnim";
import resizeHandler from "../../Util/resizeHandler";
import DropDown from "../../Util/DropDown";
import History, {createLink} from "../../Util/History";
import observeSize from "../../Util/observeSize";
import SiteColors from "../../SiteColors";
import onThrashingEnd from "../../Util/onThrashingEnd";

import "./Timeline.scss";

// const TitleFill = "#831F34";
// const TimelineStroke = "#2F5451";

const timelinePoint = div("", {
	innerHTML: `<svg xmlns="http://www.w3.org/2000/svg" width="75" height="50" version="1.1">
	  <circle cx="25" cy="25" r="10"/>
	  <path d="M 25 15 v -15"/>
	  <path d="M 25 35 v 15"/>
	  <path d="M 35 25 h 40"/>
	</svg>`,
}).firstChild;

const caretSvg = div("", {
	innerHTML: `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="8">
	  <path d="M 0 0 L 8 8 L 16 0"/>
	</svg>`,
}).firstChild;

// const randomCurfs: Curf[] = [
// 	{width: 12.5, height: 12.5},
// 	{width: 12.5, height: 25},
// 	{width: 25, height: 12.5},
// 	{width: 25, height: 25},
// ];

export default class Timeline {
	public readonly domNode: HTMLElement;
	private $title: HTMLElement;
	private $lifeEventList: HTMLElement;
	private titleDropDown: DropDown;
	private filteringTag: Tags;
	private tagToDropdownItemMap: Map<Tags, HTMLElement>;
	private lifeEventToNodeMap: Map<ILifeEvent, HTMLElement>;

	constructor() {
		this.tagToDropdownItemMap = new Map();
		this.lifeEventToNodeMap = new Map();
		this.domNode = div("Timeline", {id: "Timeline"}, [
			this.getnitTitle(),
			this.getnitLifeEventList(),
		]);

		History.searchArgs.observe((searchArgs) => {
			this.setFilteringTag(searchArgs.projects || "featured" as any);
		});
	}

	private getnitTitle() {
		if (this.$title === undefined) {
			this.$title = div("Title", [
				div("Text", "Projects Timeline"),
				div("Filter", [
					span("Label", "Filter by Tag: "),
					() => {
						const tagItems = tagList.sort().map((tag) => {
							const link = createLink(`Tag`, {href: `?projects=${tag}#Timeline`}, tag)
							this.tagToDropdownItemMap.set(tag, link);
							return link;
						});
						this.titleDropDown = new DropDown(tagItems);
						const curfAug = new ScalableCurfAug(this.titleDropDown.domNode, {
							curfSw: {width: 12.5, height: 12.5},
							curfSe: {width: 25, height: 25},
							fillStyle: SiteColors.timelineTitleBg,
							strokeWidth: 3,
							strokeStyle: "#000",
						});

						return this.titleDropDown.domNode;
					},
				]),
			]);

			let minOffsetTop = -1;
			resizeHandler(() => { minOffsetTop = -1; });

			let requestedFrame: number;
			window.addEventListener("scroll", (event) => {
				if (requestedFrame) { return; }
				requestedFrame = requestAnimationFrame(() => {
					if (minOffsetTop === -1) { minOffsetTop = this.$title.offsetTop; }
					minOffsetTop = Math.min(this.$title.offsetTop, minOffsetTop);
					if (window.scrollY >= minOffsetTop) {
						this.$title.classList.add("--stuck");
					} else {
						this.$title.classList.remove("--stuck");
					}
					requestedFrame = undefined;
				});
			});

			const titleCurfAug = new ScalableCurfAug(this.$title, {
				curfSw: {width: 12.5, height: 12.5},
				// curfSe: {width: 25, height: 25},
				fillStyle: SiteColors.timelineTitleBg,
			});
		}
		return this.$title;
	}

	private getnitLifeEventList() {
		if (this.$lifeEventList === undefined) {
			this.$lifeEventList = div("LifeEventList", [
				div("TimelinePadding top"),
				...LifeEventList.map((lifeEvent) => {
					const {title, startDate, endDate, tags, desc} = lifeEvent;
					let $info: HTMLElement;
					const $lifeEvent = div("LifeEvent", [
						div("TimelineGraph", [
							// div("Date", endDate ? ([simplifyDate(startDate), br(), "to", br(), simplifyDate(endDate)]) : simplifyDate(startDate)),
							div("Date", endDate ? simplifyDate(endDate) : simplifyDate(startDate)),
							div("GraphLine"),
							addClass(timelinePoint.cloneNode(true) as any, "GraphPoint"),
							div("GraphLine"),
						]),
						(new GridSizingWrapper(
							$info = div("Info", [
								div("PermalinkTarget", {id: lifeEvent.id}),
								div("Title", [ 
									title,
									createLink("Permalink", {href: `#${lifeEvent.id}`}, "#"),
								]),
								div("Desc", (() => {
									const descOut = desc();
									return Array.isArray(descOut) ? descOut : [descOut];
								})()),
								div(`Tags`, tags.map((tag) => createLink(`Tag ${tag}`, {href: `?projects=${tag}#Timeline`}, tag))),
							]),
						)).domNode,
					]);
					this.lifeEventToNodeMap.set(lifeEvent, $lifeEvent);

					const curfAug = new ScalableCurfAug($info, {
						strokeWidth: SiteColors.timelineStrokeWidth,
						strokeStyle: SiteColors.timelineStroke,
						fillStyle: SiteColors.timelineFill,
						curfNw: {width: 15, height: 20},
						curfNe: {width: 5, height: 10},
						curfSw: {width: 35, height: 40},
						curfSe: {width: 25, height: 30},
					})
					return $lifeEvent;
				}),
				div("TimelinePadding bottom"),
			]);

			onThrashingEnd(this.$lifeEventList, () => {
				const glowyGrid = new GlowyGridAnim({
					auraRadius: 400,
					auraColor: "#4AF",
					mouseMoveListenerTarget: this.$lifeEventList,
				});

				this.$lifeEventList.insertBefore(glowyGrid.domNode, this.$lifeEventList.firstChild);
			});
		}
		return this.$lifeEventList
	}

	private setFilteringTag(tag: Tags) {
		if (this.filteringTag === tag) { return; }
		this.filteringTag = tag;

		this.titleDropDown.selectChild(this.tagToDropdownItemMap.get(tag));
		Array.from(this.lifeEventToNodeMap.keys()).forEach((lifeEvent) => {
			const lifeEventNode = this.lifeEventToNodeMap.get(lifeEvent);
			lifeEventNode.style.display = lifeEvent.tags.indexOf(tag) !== -1 ? "" : "none";
		});
	}
}


const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
function simplifyDate(date: Date): string {
	return months[date.getMonth()] + " " + date.getFullYear();
}

