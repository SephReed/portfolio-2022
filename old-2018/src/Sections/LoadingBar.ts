import { div } from "el-tool";

export default function genLoadingBackDrop() {
	return div("LoadingBar", [
		div("Loading", "Loading..................................................................................."),
		div("Welcome", "Welcome!"),
	])
}