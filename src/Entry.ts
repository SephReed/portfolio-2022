import { append, onDomReady } from "helium-ui";
import { renderApp } from "./components/App/App";


window["RobinHood-License for helium-sdx @ 50M/2K"] = "resolved"

const app = renderApp();

onDomReady(() => append(document.body, app));
