import {div, externalLink} from "helium-ui";
import {ILifeEvent, twoColLayout, extLink, fromMD} from "./LifeEvent"


const JobEvents: ILifeEvent[] = [];



/*****************
*.  CODER
******************/
JobEvents.push({
	id: "CoderComDev",
	startDate: new Date("Jan 15 2018"),
	endDate: new Date("Sep 25 2018"),
	tags: ["featured", "work", "development", "design", "audio"],
	title: "Front-End Developer at Coder Technologies",
	shortDesc: "At a tech start-up, assigned to develop a browser-embedded IDE and a CRUD site; ended up filling a bunch of other roles.",
	images: [{ contain: "CoderLogo(square).png" }, "CoderInterior.jpg"],
	skills: ["Typescript", "Sass", "React", "Node.js", "npm", "git", "Vanilla Js", "Audio Editing", "Documentation", "Storyboarding"],
	desc: () => fromMD(`

**Info:** When I started at Coder there were less than a dozen employees, so I got to fill many roles besides front-end developer.  Our mission was to create a collaborative cloud based software development suite, including in browser IDE and remote compilation/storage/hosting. Some of the projects I did include:
<br><br>
- Site wide theming, procedurally generated from Textmate/Bash themes (see Lumas)
- Many internal pages of outstanding quality (as said by the designers and QA)
- Major site components such as Button, Link, EditableText, ValidatorInput, PageTween, MinimizableDiv, and Modal
- A keyboard navigation system for the IDE
- Background/intro/outro music for company promo/tutorial vids
- Original concepting for landing page
- Some major influences in our workflow and timeline
- Head of Easter Eggs
- A few very nice blog posts
----------
<div class="Links">
	${extLink(
		"Coder.com *",
		"http://coder.com",
	)}
	<br>
	${extLink(
		"Watch product video I made the music for",
		"https://www.youtube.com/watch?v=CMKRbc8DpVs",
	)}
	<br>
	${extLink(
		"Link to song from video above",
		"https://soundcloud.com/thumbz/forestbackground",
	)}
</div>

<small>\* Due to massive front-end refactoring, there is very little to show of my work</small>
	`),
});

/*********************
*   DAD LIGHT
**********************/
JobEvents.push({
	id: "DadLight",
	startDate: new Date("Jan 21 2017"),
	endDate: new Date("Aug 11 2017"),
	tags: ["featured", "work", "development", "fabrication", "audio"],
	title: `Independent Contractor for Project "Dad Light"`,
	skills: ["Web Workers", "Linux", "Chip Design", "Node Js", "Signal Processing", "Carpentry", "Mobile App"],
	images: ["DadLight-Glamor(loop).gif", "DadLight-Internal.jpg"],
	shortDesc: "A custom built audio-reactive ceiling light fixture, with an app for control.",
	desc: () => fromMD(`
Sally Hall was just finishing up her classes at Austin Center for Design and had a wonderful idea of a gift to make her father.  The basic premise was this:  Her father was an audiologist, so she wanted to make him a ceiling light which responded to sound similar to the way the hairs in our ears do while playing music.  She spread this idea broadly, looking for someone who could make it happen (on a college student's budget), and found me.  We met over coffee and discussed its feasibility, then I used sketchup to design the project alongside her.  What we came up with was a seamless box containing a raspberry pi, break-out board, and power supply that would hang from the ceiling.  Extending out of this box was twelve hanging jar lamps with fairy lights inside, each lamp representing a tone from the western 12 tone scale.  In order to make the lights play music, I developed a webapp which was served from the pi, in which Sally could choose songs, run patterns, turn on mic input, and control things like brightness, on/off, and fade time.  The hardest parts of this project were getting the FFT to be performant, and designing the 12 female-USBs breakout board which had to use MOSFETs to step up the pi's 3v3 GPIOs to the 5v power supply for the lights.
	`),
});



/*********************
*   STASH CRYPTO
**********************/
JobEvents.push({
	id: "Stash",
	startDate: new Date("Oct 8 2016"),
	tags: ["featured", "work", "development", "design"],
	title: "Web Developer at Stash Crypto Inc",
	images: [{ contain: "StashLogo.png" }, "stash_node_site.gif", "digiden_loop.gif"],
	shortDesc: "Sole developer for various web services related to a crypto cold-wallet; also did some 3d product modeling and renders.",
	skills: ["Vanilla Js", "3d Modeling", "Local Storage", "Security", "Cryptology"],
	links: () => [
		externalLink({
			innards: "StashCrypto.com",
			href: "https://stashcrypto.com",
		}),
		externalLink({
			innards: "View product page prototype",
			href: "http://sephreed.github.io/stash_node/index.html",
		}),
		externalLink({
			innards: "View digital identity search engine prototype",
			href: "http://sephreed.github.io/Digiden/index.html",
		}),
		externalLink({
			innards: "Cryptopals crypto challenges",
			href: "https://cryptopals.com/",
		}),
	],
	desc: () => fromMD(`

I met the CEO of Stash Crypto at a crypto-currency meetup held at the Factom offices, and we hit it off pretty well.  He happened to be in need of a web developer at the time.  The company direction was often tumultuous, leading ultimately to the disposal of the company identity in an attempt to mimic Apple (a common story in start-ups).  I made a 3d model of our original logo and our flagship product, as well as a website for an internet alliance they were attempting to start, prototypes for the digital identity search engine (with AES-EBC encrypted local storage of searches) and the product landing page (which includes the product 3d renders).  While working there, I started to really study security and cryptology, specifically through the cryptopals challenges.
		`),

});

// /*********************
// *   TEMPLATE
// **********************/
// JobEvents.push({
// 	id: "Stash",
// 	startDate: new Date("Oct 8 2016"),
// 	tags: ["featured", "work", "development", "design"],
// 	title: "",
// 	desc: twoColLayout(
// 		[
// 			{ hackableHTML: extLink(
// 				`<img class="--clip_curf_all_sm --add_bg_light" src="${('../../../../assets/StashLogo.png')}">`,
// 				"https://stashcrypto.com",
// 			)},
// 		],
// 		//
// 		fromMD(`
// **Skills:**

// ----

// **Info:**

// ---

// <div class="Links">
// 	${extLink(
// 		"StashCrypto.com",
// 		"https://stashcrypto.com",
// 	)}
// </div>
// 		`),
// 	),
// });

export default JobEvents;
