import {div, externalLink} from "helium-ui";
import {ILifeEvent, twoColLayout, extLink, fromMD} from "./LifeEvent"


const CodeEventList: ILifeEvent[] = [];



/*********************
*   THIS PORTFOLIO
**********************/
CodeEventList.push({
	id: "Portfolio2018",
	startDate: new Date("Oct 3 2018"),
	tags: ["featured", "development", "design", "imagery", "writing", "interactive"],
	title: "2018 Portfolio",
	shortDesc: `Previous portfolio, utilizing an early prototype of my web framework Helium-UI`,
	desc: () => [
		fromMD(`
The web-app you're currently looking at is meant to exemplify my take on modern visual and systems design, as well as an excuse to do some fun stuff I wouldn't normally do at work.  Everything on this page was created by me from scratch, from the interactive art board at the top of the page, to the grid tools which keep all items on the timeline flush with the grid, to the mono-line icons at the bottom of the page in the contact area.  The site was coded in TypeScript and Sass, rendered with "el-tool", and bundled with webpack.  It is also fully responsive, and has been tested on Firefox, Chrome, and Safari.

-----

<div class="Links">
	${extLink(
		"PDF version of this portfolio",
		"http://sephreed.me/SephReed-Portfolio.pdf",
	)}
	<br>
	${extLink(
		"Link to Resume",
		"http://sephreed.me/SephReed-Resume.pdf",
	)}
	<br>
	${extLink(
		"Link to Resume (printer friendly version)",
		"http://sephreed.me/SephReed-Resume(printer).pdf",
	)}
</div>
`
		),
	],
});



// /*********************
// *   NPM Tools
// **********************/
// CodeEventList.push({
// 	id: "NobleJs",
// 	startDate: new Date("Jul 1 2018"),
// 	tags: ["featured", "development", "writing"],
// 	title: "Noble.js",
// 	desc: fromMD(
// `Noble.js is a modular web toolkit which has already shown some very promising results.  The current modules include:

// - el-tool: a web rendering tersifier which can render interactive sites faster than any other client side renderer (including react, vue, and angular).  It has less than 250 lines of unminified/commented code.
// - vanilla-observables: similar to mobx or redux, this tool adds the ability to observe properties on a class, or just create an observable store.  It has less than 100 lines of unminified/commented code which could have been run in 2008.
// - observable-location(unpublished): window.location, but with observable properties.  Includes a helper function for generating non-redirecting ADA compliant links. It has less than 100 lines of unminified/commented code.
// - omject: an abstract base class meant for object model systems.  Replaces globals and pass-downs by giving classes knowledge of their parent class.  Allows for bubbling events upwards, or use of "getAncestor(ClassType)" to access larger scopes directly.  It has less than 125 lines of unminified/commented code.
// - disinteractive-observer(unpublished): allows for the ability to observe whether or not an element is disinteractiveed on page.
// - vanilla-component(unpublished): combines all of the above to make an HTMLElement manager that atatches/detaches observers on show/hide, can have observable properties, and renders incredibly fast.

// The objective of this toolkit is to illustrate just how bloated and overly complicated most of popular tools are these days.  It is named after noble gasses beacuse they are non-reactive.
// `),
// });


/*********************
*   PINE PORTFOLIO
**********************/
CodeEventList.push({
	id: "PINEPortfolio",
	startDate: new Date("Dec 29 2015"),
	endDate: new Date("Jan 15 2018"),
	tags: ["featured", "development", "design", "imagery", "writing", "3d", "interactive"],
	title: "2015 Portfolio",
	shortDesc: `My first portfolio, using my first custom (Vue-like) web framework "PINE" and some 3d animations.`,
	skills: ["Vanilla JavaScript", "Three.js", "HTML Canvas"],
	links: () => [
		externalLink({
			href: "http://sephreed.github.io/portfolio/index.html",
			innards: "Check out my github.io portfolio",
		})
	],
	images: [{ contain: "PortfolioScreen-2014.png" }],
	desc: () => twoColLayout(
		{ hackableHTML: extLink(
			`<img class="--clip_curf_ne_sw" src="http://sephreed.github.io/public/images/PortfolioScreen.png">`,
			"http://sephreed.github.io/portfolio",
		)},
		fromMD(`While there's no doubt that my newer portfolio is much more professional, I still can't decide whether I personally like it more than my old one.  The old one was built off my first web framework (called PINE), and has a ton of really interactive qualities to it that just make me smile.  There's a manipulable 3d model at the top, some classic bumpy gradients, a 3d star field, and some high-res background nonsense.  Worth checking out IMO.`)
	),
});


/*********************
*   DOCURIZER
**********************/
CodeEventList.push({
	id: "Docurizer",
	startDate: new Date("Oct 20 2017"),
	tags: ["featured", "development", "design"],
	title: "Docurizer",
	skills: ["Node.js", "Abstract Syntax Trees", "C++", "RegExp", "Documentation"],
	images: ["Docurizer_overview.gif"],
	shortDesc: `A documentation generator built to dissect an open-source C++ project that had no docs.`,
	links: () => [
		externalLink({
			href: "http://sephreed.github.io/portfolio/Docurizer_Render/index.html?file=main.cpp.html&ov=inc",
			innards: "View Docurizers rendered output (try inspecting elements)",
		}),
		externalLink({
			href: "https://github.com/SephReed/Servers/blob/5e7984405e47587f234679b8bc0e114824e7da13/Docurizer/libs/Scoperizer/scope_defs/cpp/scopes.js",
			innards: "View C++ grammar for Scopifier",
		}),
		externalLink({
			href: "https://github.com/SephReed/Servers/tree/master/Docurizer",
			innards: "View project source code",
		})
	],
	desc: () =>
		fromMD(`
	In order to make sense out of an undocumented open source project, I built these two tools to parse the project and automatically generate docs based off any comments it could find laying around.  A year later, I discovered that when I created Scopifier I'd inadvertantly reinvented grammars.  As for Docurizer, it takes a symbolic tree generated by applying a grammar, reorganizes the information, then renders it as html.`
	),
});



/*********************
*   FLAT WORLD
**********************/
CodeEventList.push({
	id: "FlatWorld",
	startDate: new Date("Jul 3, 2017"),
	endDate: new Date("Sep 14, 2017"),
	tags: ["featured", "development", "design", "imagery", "interactive"],
	title: "FLAT.World",
	skills: ["Single Page Web App", "Node.js", "Servers", "HTML5 Canvas"],
	images: ["flatWorld_loop.gif"],
	links: () => [
		externalLink({
			href: "http://sephreed.github.io/FlatStory/index.html",
			innards: "Play with live demo (no server access)",
		}),
		externalLink(
			"View source code",
			"https://github.com/SephReed/SephReed.github.io/tree/master/FlatStory",
		)
	],
	shortDesc: `My first attempted browser-based 2d game maker. Inspired by RPG Maker 2000 (the application I first learned to program in), I was hoping to mimic the way it makes coding feel more accessible to new comers.`,
	desc: () => fromMD(`
FLAT.World was my first attempted open source in browser 2d game maker. Inspired by RPG Maker 2000 which was the application I first learned to program in, I was hoping to mimic the style it has which makes coding feel more accessible to new comers.
	`),
});




/*********************
*   3d Walkscape
**********************/
CodeEventList.push({
	id: "3dWalkscape",
	startDate: new Date("Oct 8 2016"),
	tags: ["featured", "development", "3d", "interactive"],
	title: "3d Walkscape",
	shortDesc: "My first time messing with webgl, specifically Three js.  I was attempting to build an explorable scenic cabin in the mountains.",
	skills: ["Three.js", "Blender", "Game Engines", "Procedural Generation", "SketchUp"],
	images: ["3d_walkscape_loop.gif"],
	links: () => [
		externalLink({
			href: "http://sephreed.github.io/walkscape/index.html",
			innards: "Play with live demo",
		}),
		externalLink({
			href: "https://www.youtube.com/watch?v=Cu_6l9h4BIs&t=30s",
			innards: "Watch screencap",
		})
	],
	desc: () => fromMD(` This was a fairly simple project, testing out Three.js to see what it was capable of.  The answer is quite a lot for anything in a browser.  The project has some very rough physics (the collisions are done with raycasting), perlin noise procedurally generated hills (no random seed), and a few interactive elements (the door to the house and the sphere).  I'd like to get back into this project some day.
	`),
});

// /*********************
// *   TEMPLATE
// **********************/
// CodeEventList.push({
// 	id: "Stash",
// 	startDate: new Date("Oct 8 2016"),
// 	tags: ["featured", "work", "development", "design"],
// 	title: "",
// 	desc: twoColLayout(
// 		[
// 			{ hackableHTML: extLink(
// 				`<img class="--clip_curf_all_sm --add_bg_light" src="${('../../../../assets/StashLogo.png')}">`,
// 				"https://stashcrypto.com",
// 			)},
// 		],
// 		//
// 		fromMD(`
// **Skills:**

// ----

// **Info:**

// ---

// <div class="Links">
// 	${extLink(
// 		"StashCrypto.com",
// 		"https://stashcrypto.com",
// 	)}
// </div>
// 		`),
// 	),
// });

export default CodeEventList;
