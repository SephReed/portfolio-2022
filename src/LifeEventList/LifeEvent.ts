import {  div, Innards, RawHTML } from "helium-ui";
// import { markdown } from "markdown";
import * as MarkdownIt from "markdown-it";
const MD = MarkdownIt({
	html: true,
	xhtmlOut: true,
});

export type Tags = "featured" | "design" | "fabrication" | "development" | "audio"
| "imagery" | "social" | "leadership" | "writing" | "3d" | "interactive" | "work";

const tagsObject: {[key in Tags]: null} = {
	featured: null,
	design: null,
	fabrication: null,
	development: null,
	audio: null,
	imagery: null,
	social: null,
	leadership: null,
	writing: null,
	"3d": null,
	interactive: null,
	work: null,
}
const tagList: Tags[] = Array.from(Object.keys(tagsObject)) as any;
export { tagList };

export interface ILifeEvent {
	id: string;
	startDate: Date,
	endDate?: Date,
	tags: Tags[];
	title: HTMLElement | string;
	desc: () => (Innards);
	shortDesc?: Innards;
	links?: () => Innards;
	images?: Array<string | { contain: string} | HTMLImageElement>;
	skills?: string[];
}

export function fromMD(str: string): RawHTML {
	return {hackableHTML: MD.render(str)};
}

export function extLink(innerText: string, href: string) {
	return `<a target="_blank" href=${href}>${innerText}</a>`;
}

export function twoColLayout(left: Innards, right: Innards) {
	return div("TwoColLayout", [
		div("LeftCol", Array.isArray(left) ? left : [left]),
		div("RightCol", Array.isArray(right) ? right : [right])
	]);
}
