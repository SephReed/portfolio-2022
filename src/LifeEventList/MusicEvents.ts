import {div, externalLink} from "helium-ui";
import {ILifeEvent, twoColLayout, extLink, fromMD} from "./LifeEvent"


const MusicEvents: ILifeEvent[] = [];

/*********************
*   BEST 40
**********************/
MusicEvents.push({
	id: "WellConditionedLover",
	startDate: new Date("Sep 11 2014"),
	tags: ["featured", "audio", "imagery"],
	title: "Album - A Well Conditioned Lover",
	images: [{ contain: "https://f4.bcbits.com/img/a1155412321_16.jpg" }],
	skills: ["Synths", "Jazz Theory", "Mixing/Mastering", "Album Cover/Merch Drawing"],
	shortDesc: "Third album; a collection of more happy and uptempo pieces.",
	links: () => [
		externalLink({
			href: "https://thumbz.bandcamp.com/album/a-well-conditioned-lover",
			innards: "Listen/Free Download of Album",
		})
	],
	desc: () => fromMD(`
The title is ironic, meant to be a parody of the audacious tactics of advertising.  This was my third album, built of my more happy and poppy tunes.  It takes influence from trance, house, jazz, theater, and video game music.  Most of the songs on this album were produced at the same time as the ones from my second album, but were thematically out of place on that album.  The cover art is meant to represent the paths of the mind (logic/abstraction) and the gut (emotion/deep learning), and the human endeavor to lead them to each other.  This was the kind of heady stuff I was on about when writing most of these songs.


		`),
});



/*********************
*   DAWN OF SENTIENCE
**********************/
MusicEvents.push({
	id: "DawnOfSentience",
	startDate: new Date("Aug 28 2014"),
	tags: ["featured", "audio", "imagery"],
	title: "Album - The Dawn of Sentience",
	desc: () => twoColLayout(
		[
			{ hackableHTML: extLink(
				`<img class="--clip_curf_all_sm --add_bg_light" src="https://f4.bcbits.com/img/a2388824960_16.jpg">`,
				"https://thumbz.bandcamp.com/album/the-dawn-of-sentience",
			)},
		],
		//
		fromMD(`
**Skills:** Instrument Synthesis, Jazz Theory, Mixing/Mastering, Album Cover/Merch Drawing

----

**Info:**  This was my third album, built of my slower and darker tunes.  It takes influence from early uk dubstep, hip-hop, jazz, theater, and video game music.  This album is dedicated to sentience in humanity, may we one day find ourselves a culture rich with appreciation for all the beautiful wonders of the universe we've been gifted access to.  May we one day live comfortably amongst the stars.

---

<div class="Links">
	${extLink(
		"Listen/Free Download of Album",
		"https://thumbz.bandcamp.com/album/the-dawn-of-sentience",
	)}
</div>
		`),
	),
});

// /*********************
// *   TEMPLATE
// **********************/
// MusicEvents.push({
// 	id: "Stash",
// 	startDate: new Date("Oct 8 2016"),
// 	tags: ["featured", "work", "development", "design"],
// 	title: "",
// 	desc: twoColLayout(
// 		[
// 			{ hackableHTML: extLink(
// 				`<img class="--clip_curf_all_sm --add_bg_light" src="${('../../../../assets/StashLogo.png')}">`,
// 				"https://stashcrypto.com",
// 			)},
// 		],
// 		//
// 		fromMD(`
// **Skills:**

// ----

// **Info:**

// ---

// <div class="Links">
// 	${extLink(
// 		"StashCrypto.com",
// 		"https://stashcrypto.com",
// 	)}
// </div>
// 		`),
// 	),
// });

export default MusicEvents;
