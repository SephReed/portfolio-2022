import { Sourcify, UrlState, noDeps, HeliumRouteState } from "helium-ui";

UrlState.preventRefreshWarningGiven = true;

export type RouteParamId = "sidebar";


export interface IRouteConfig {
	id: string;
	test: null | string | RegExp;
	paramId?: RouteParamId;
	getUrl?: string | (() => string);
	// icon?: IconType;
}


const RouteConfigs = {
	"presence": {
		test: "/presence"
	},
	"root": {
		test: /^\/?$/,
		getUrl: () => "/"
	},
	"portfolio": {
		test: "/portfolio",
	},
// } as Record<string, Omit<IRouteConfig, "id">>;
} as const;

export type RouteId = keyof typeof RouteConfigs;


export const UrlParamList = [
	"email",
	"userId",
	"secret"
] as const;

export type UrlParam = typeof UrlParamList[number];

export class RouteState extends HeliumRouteState<RouteId, IRouteConfig, UrlParam> {
	constructor() {
		super(RouteConfigs)
	}
}
