import { Source, Sourcify } from "helium-sdx";



export const FilterTagList = [
	"2d design",
	"3d design",
	"collaborative",
	"code",
	"circuits",
	"fabrication",
	// "featured",
	"interactive",
	"leadership",
	"sound",
	// "writing",
] as const;


export type FilterTag = typeof FilterTagList[number];


export const FilterPresetIdList = [
	"all",
	"tech",
	"art",
	"leadership",
] as const;

export type FilterPresetId = typeof FilterPresetIdList[number];

export const FilterPresets: Array<{
	id: FilterPresetId;
	tags: "all" | FilterTag[];
}> = [
	{ id: "all",
		tags: "all",
	},
	{ id: "art",
		tags: []
	},
	{ id: "tech",
		tags: ["code", "circuits"],
	},
	{ id: "leadership",
		tags: ["leadership","collaborative"],
	},
]

export class EventFilterState {
	protected state = Sourcify({
		presetId: null as null | FilterPresetId,
		tags: [] as "all" | FilterTag[],
	});


	public get presetId() { return this.state.presetId; }

	public set presetId(id: FilterPresetId | null) {
		console.log(id);
		if (id === null) {
			this.clear();
			return;
		}

		const preset = FilterPresets.find((it) => it.id === id);
		this.state.presetId = preset.id;
		const { tags } = preset;
		const items = Array.isArray(tags) ? [...preset.tags] as FilterTag[] : tags;
		this.state.set("tags", items);
	}

	public clear() {
		this.state.upsert({presetId: null, tags: null });
	}
}
