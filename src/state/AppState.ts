import { derive } from "helium-sdx";
import { EventFilterState } from "./EventFilterState";
import { RouteState } from "./RouteState";

export class AppState {
	public readonly eventFilter = new EventFilterState();
	public readonly route = new RouteState();

	constructor() {
		derive(() => {
			if (this.route.id.startsWith("portfolio")) {
				const preset = this.route.getPathSlice(1);
				this.eventFilter.presetId = (preset || null) as any;
			}
		})
	}
}

export const APP = new AppState();
