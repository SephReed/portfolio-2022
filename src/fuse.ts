import { fusebox, sparky } from "fuse-box";
import { join } from 'path';

const bundler = fusebox({
	target: "browser",
	webIndex: {
		template: './template.html'
	},
	entry: "./Entry.ts",
	cache: false,
	devServer: {
		httpServer: {
			enabled: true,
			port: 7773
		}
	},
	// stylesheet: {
  //   autoImport: [{
  //     file: "./src/styling/vars.scss"
  //   }]
  // },
	// watcher: {
	// 	root: "../",
	// 	include: [
	// 		join(__dirname, "./src"),
	// 		join(__dirname, "../client/src"),
	// 		// join(__dirname, "../timeline/src"),
	// 		join(__dirname, "../middleware/src"),
	// 		// join(__dirname, "../utils/src"),
	// 	]
	// }
});


bundler.runDev();






class Context {
  isProduction;
  runServer;
}
const { task, src } = sparky<Context>(Context);

task("default", async () => {
  await src("../assets/**/**.**")
    .dest("./dist/assets/", "/assets/")
    .exec();

  // await src("./src/pages/Articles/md/**/**.md")
  //   .dest("./dist/articles/", "/Articles/md/")
  //   .exec();
});
