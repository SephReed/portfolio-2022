import "./topbar.scss";

import { anchor, byId, div } from "helium-ui";
import { APP } from "src/state/AppState";
import { RouteId } from "src/state/RouteState";

export function renderTopbar() {
	const links: Array<{routeId: RouteId, name: string}> = [
		{ routeId: "portfolio",
			name: "Portfolio",
		},
		{ routeId: "root",
			name: "Resumes",
		},
		{ routeId: "presence",
			name: "Web Presence",
		},
	];

	return div("Topbar", [
		...links.map((it) => anchor({
			href: APP.route.getUrlForRoute(it.routeId),
			ddxClass: () => APP.route.id === it.routeId && "--selected",
			innards: it.name,
			onPush: () => byId("Mountains").scrollIntoView({
				block: "nearest",
				behavior: "smooth"
			})
		})),
		div("Contact", "Contact")
	])
}
