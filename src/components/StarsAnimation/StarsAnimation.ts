import * as THREE from "three";



export class StarsAnimation {
  constructor(protected args?: {
    rotationScale?: number;
    cameraMoveSpeed?: number;
		mousemoveTarget?: HTMLElement;
  }) {
		this.args = {
			rotationScale: 0.3,
			cameraMoveSpeed: 0.003,
			...this.args
		}
  }

  protected _domNode: HTMLElement;
  public get domNode() {
    if (!this._domNode) {
      this._domNode = document.createElement("div");
      this._domNode.classList.add("StarsAnimation")
    }
    return this._domNode;
  }

  protected threeEntities: {
    renderer: THREE.WebGLRenderer;
    camera: THREE.PerspectiveCamera;
    scene: THREE.Scene;
  };
  protected state = {
    mouseX: 0,
    mouseY: 0
  }

  public init() {
    if (this.threeEntities) { return; }

		const mousemoveTarget = this.args.mousemoveTarget || this.domNode;
    mousemoveTarget.addEventListener("mousemove", ({x, y}) => {
      const { domNode } = this;

      const width = domNode.offsetWidth;
      const height = domNode.offsetHeight;

      this.state.mouseX = x - (width/2);
      this.state.mouseY = y - (height/2);
    });


    // console.log("Space container dimensions: "+width +"x"+height);

    const renderer = new THREE.WebGLRenderer({antialias: true, alpha: true});
    this.domNode.appendChild( renderer.domElement );

    const camera = new THREE.PerspectiveCamera(100, 1, 1, 2000);
    camera.position.z = 1000;

    const scene = new THREE.Scene();
    scene.fog = new THREE.FogExp2(0x000000, 0.0007);

    this.threeEntities = {
      camera,
      renderer,
      scene
    }

    this.updateSize();


    // const positions = geometry.attributes.position.array as number[];
    const points: THREE.Vector3[] = [];

    for (let i = 0; i < 200; i++) {
      points.push(new THREE.Vector3(
        Math.random() * 2000 - 1000,
        Math.random() * 2000 - 1000,
        Math.random() * 2000 - 1000
      ))
    }

    const geometry = new THREE.BufferGeometry().setFromPoints(points);

    const materials: THREE.PointsMaterial[] = [];
    const parameters: Array<{color: [number, number, number], size: number}> = [
      { color: [1, 1, 0.5], size: 5},
      { color: [0.95, 1, 0.5], size: 4},
      { color: [0.90, 1, 0.5], size: 3 },
      { color: [0.85, 1, 0.5], size: 2},
      { color: [0.80, 1, 0.5], size: 1},
    ]

    parameters.forEach(({color, size}) => {
      const addMe = new THREE.PointsMaterial({
        size: size,
        opacity: 0.5
      });
      materials.push(addMe);

      const particles = new THREE.Points(geometry, addMe);

      particles.rotation.x = Math.random() * 6;
      particles.rotation.y = Math.random() * 6;
      particles.rotation.z = Math.random() * 6;

      scene.add(particles);
    })

    // domBox.addEventListener('mousemove', onDocumentMouseMove, false);
    // window.addEventListener('resize', onWindowResize, false);

  }

  public updateSize() {
    console.log(this.domNode);
    const width = this.domNode.offsetWidth;
    const height = this.domNode.offsetHeight;

    this.threeEntities.camera.aspect = width / height;
    this.threeEntities.camera.updateProjectionMatrix();

    this.threeEntities.renderer.setSize(width, height);
  }

  public drawFrame() {
    const time = Date.now() * 0.00005;

    const { camera, scene, renderer } = this.threeEntities;
    const { mouseX, mouseY } = this.state;
    const { cameraMoveSpeed, rotationScale } = this.args;
    camera.position.x += (-mouseX - camera.position.x) * cameraMoveSpeed;
    camera.position.y += (mouseY - camera.position.y) * cameraMoveSpeed;

    camera.lookAt(scene.position);

    scene.children.forEach((child, i) => {
      if (child instanceof THREE.Points) {
        child.rotation.y = time * (i < 4 ? i + 1 : -(i + 1)) * rotationScale;
      }
    })

    renderer.render(scene, camera);
  }

  public play() {
    if (this.frameRequest) { return; }
    this.init();
    this.animate();
  }

  public pause() {
    if (!this.frameRequest) { return; }
    cancelAnimationFrame(this.frameRequest);
  }


  protected frameRequest: any;
  protected animate() {
    this.frameRequest = requestAnimationFrame(() => this.animate())
    this.drawFrame();
  }
}
