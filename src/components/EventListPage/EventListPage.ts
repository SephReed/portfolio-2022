import "./eventList.scss";

import { anchor, br, div, externalLink, img } from "helium-ui";
import { APP } from "src/state/AppState";
import { renderEventFilter } from "../EventFilter/EventFilter";
import { renderHeader } from "../Header/Header";
import LifeEventList from "src/LifeEventList";



export function renderEventListPage() {
	return div("EventListPage", [
		renderEventFilter(),
		renderEventList(),
	])
}




export function renderEventList() {
	return div("EventList", [
		// div("Background",[
		// 	div("Sticky", [
		// 		div("Forest"),
		// 		div("Hex"),
		// 	])
		// ]),
		div("Foreground", LifeEventList.map((ev) =>
			div("LifeEvent", [
				div("Title", ev.title),
				br(0.5),
				div("ShortDesc", ev.shortDesc),
				br(0.5),
				ev.skills && div("Skills", [
					"Skills Utilized: ",
					ev.skills.join(", ")
				]),
				ev.images && div("Images", {
					ddxClass: [
						() => ev.images.length <= 1 && "singleColumn",
						() => ev.images.length === 3 && "centerThird",
					]
				},ev.images.map((image) => {
					let src: string;
					if (image instanceof HTMLImageElement) {
						return image;
					}
					const name = typeof image === "string" ? image : image.contain;
					if (name.startsWith("http")) {
						src = name;
					} else {
						let path = "/assets/";
						if (name.endsWith(".gif")) {
							path += "gif/"
						} else if (name.endsWith(".svg")) {
							path += "svg/"
						} else {
							path += "img/500/"
						}
						src = path + name;
					}
					return externalLink({
						href: src.replace("/500/", "/raw/"),
						style: { backgroundImage: `url("${src}")`},
						innards: img({ src }),
						ddxClass: () => typeof image !== "string" && "contain",
					}) ;
				})),
				ev.tags && div("Tags", ev.tags.map((tag) =>
					div("Tag", {
						ddxClass: () => "--selected",
						innards: tag
					})
				))
			])
		))
	])
}
