import "./header.scss";


import { addClass, append, div, el } from "helium-ui";
import { StarsAnimation } from "../StarsAnimation/StarsAnimation";
import { TangentsAnimation } from "../TangentsAnimation/TangentsAnimation";
import { renderGlowingThumbzSymbol } from "../GlowingThumbzSymbol/GlowingThumbzSymbol";




export function renderHeader() {
	return div("Header", [
		renderDivider(),
		div("MainArea", {
			ref: (mainAreaEl) => {
				append(mainAreaEl, [
					div("Foreground", [
						div("Name", [
							"SEPH ",
							renderGlowingThumbzSymbol(),
							" REED",
						])
					]),
					div("Background", [
						() => {
							const stars = new StarsAnimation({
								mousemoveTarget: mainAreaEl
							});
							setTimeout(() => stars.play());
							return stars.domNode;
						},
						div("Fog"),
						() => {
							const out = el("canvas", { class: "Tangents"});
							const tangents = new TangentsAnimation({
								canvas: out,
								mousemoveTarget: mainAreaEl
							});
							setTimeout(() => tangents.updateSize());
							return out;
						},
						div("Mountains", { id: "Mountains" }),
						div("Treeline"),
						div("NearMountain"),
					]),
				])
			}
		}),
		addClass("reverse", renderDivider()),
	])
}


function renderDivider() {
	return div("Divider", {
		// ref: (ref) =>  {
		// 	const intervalMs = 150;
		// 	const loopLength = 30* 1000;
		// 	let time = 0;
		// 	setInterval(() => {
		// 		time += intervalMs;
		// 		time %= loopLength;
		// 		const dividerBgOffset = time/loopLength * 100;
		// 		ref.style.backgroundPosition = `${dividerBgOffset}% 0%`
		// 	}, intervalMs)
		// }
	})
}
