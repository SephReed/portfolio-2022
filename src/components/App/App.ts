import "./fonts.scss";
import "./app.scss";
import "./scaling.scss";


import { div } from "helium-ui";
import { tweener } from "helium-ui-tweener";
import { renderEventList, renderEventListPage } from "../EventListPage/EventListPage";
import { renderTopbar } from "../Topbar/Topbar";
import { renderHeader } from "../Header/Header";
import { APP } from "src/state/AppState";
import renderWebPresencePage from "../WebPresencePage/WebPresencePage";




export function renderApp() {
	let cursorGradient: HTMLElement;
	return div("App", {
		// onToucherMove: ({x, y}) => {
		// 	cursorGradient.style.webkitMaskPosition = `${x - 70}px ${y - 70}px`;
		// 	// cursorGradient.style.left = x + "px";
		// }
	},[
		div("Background", [
			// div("Forest"),
			div("Hex"),
			// cursorGradient = div("HexGlow", [
			// 	// cursorGradient = div("CursorGradient")
			// ])
		]),
		renderHeader(),
		div("MainContent", [
			renderTopbar(),
			div("ExtraSpace", { id: "ContentTop"}),
			tweener({
				// resizeMs: 5000,
			}, () => {
				switch (APP.route.id) {
					case "presence": return renderWebPresencePage();
					case "portfolio": return renderEventListPage();
					case "root": return APP.route.replaceUrlToRoute("portfolio");
					default: return "404";
				}
			})
		])
	])
}
