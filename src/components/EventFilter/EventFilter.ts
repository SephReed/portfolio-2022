import "./eventFilter.scss";

import { anchor, br, div } from "helium-ui";
import { FilterPresets, FilterTag } from "src/state/EventFilterState";
import { APP } from "src/state/AppState";

export function renderEventFilter() {
	return div("EventFilter", [
		br(4),
		div("Info", "Which kind of achievements are you interested in?"),
		br(),
		div("FilterButtons", FilterPresets.map((it) => {
			let imgSrc: string, name: string;

			console.log(it.id, it);
			switch (it.id) {
				case "all": name = "Everything"; break;
				case "art": name = "Art"; break;
				case "tech": name = "Tech"; break;
				case "leadership": name = "Leadership"; break;
			}
			imgSrc = it.id;

			return anchor("FilterButton", {
				style: { backgroundImage: `url("${imgSrc}")`},
				innards: name,
				href: APP.route.getUrlForRoute("portfolio") + `/${it.id}`,
				ddxClass: () => APP.eventFilter.presetId === it.id && "--selected"
			})
		})),
		br(2),
		// div("FeaturedOnly", [
		// 	"Only show me extra special items"
		// ])
	])
}
