import * as sharp from "sharp";
import * as fs from "fs";

// sharp().resize().toFile();

let imagesFiles = fs.readdirSync("./assets/img/raw");

imagesFiles = imagesFiles.filter((image) =>
	image.endsWith(".jpg") || image.endsWith(".png")
)

console.log(imagesFiles);

// const images = imagesFiles.map((file) => fs.readFileSync("./assets/img/raw/" + file));

const images = new Map<string, {
	basePath: string;
	lastUpdate: Date;
	buffer?: any;
}>()

imagesFiles.forEach((fileName) => {
	const basePath = "./assets/img/raw/" + fileName;
	images.set(fileName, {
		basePath,
		lastUpdate: fs.statSync(basePath).mtime,
	})
})

function getImageBuffer(fileName: string) {
	const image = images.get(fileName);
	if (!image.buffer) {
		image.buffer = fs.readFileSync(image.basePath);
	}
	return image.buffer;
}

const sizes = [
	300,
	500,
	800
]


sizes.forEach((size) => {
	const sizePath = `./assets/img/${size}`;
	!fs.existsSync(sizePath) && fs.mkdirSync(sizePath, { recursive: true })
	images.forEach(({lastUpdate}, name) => {
		const fullPath = `${sizePath}/${name}`;
		try {
			const existing = fs.statSync(fullPath);
			if (existing.mtime > lastUpdate) {
				console.log(`Already up to date `, fullPath);
				return;
			}
		} catch(err) {
			console.log(`No file`, fullPath);
		}

		const buffer = getImageBuffer(name);
		sharp(buffer).resize({
			width: size,
			withoutEnlargement: true,
		}).toFile(fullPath);
	})
})
